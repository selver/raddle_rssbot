from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.chrome.options import Options

from dateutil import parser
import feedparser
import json
import pytz

import requests

def check_sub(sub, entry):
    if sub == "crimethinc":
        r = requests.get(entry['link'])
        if "/podcast/" in r.url:
            sub = "podcasts"
    return sub

def login(driver, username, password):
    # Login Script
    driver.get("http://www.raddle.me/login")

    elem = driver.find_element_by_id("login-username")
    elem.clear()
    elem.send_keys(username)

    elem = driver.find_element_by_id("login-password")
    elem.clear()
    elem.send_keys(password)

    elem.send_keys(Keys.RETURN)

    wait = WebDriverWait(driver, 30)
    wait.until(EC.title_is("Raddle"))

def start_new_thread(driver, subraddle, thread_title, content, link):
    driver.get("http://www.raddle.me/submit/" + subraddle)

    wait = WebDriverWait(driver, 30)
    wait.until(EC.element_to_be_clickable((By.ID, "submission_title")))

    elem = driver.find_element_by_id("submission_title")
    elem.clear()
    elem.send_keys(thread_title)

    elem = driver.find_element_by_id("submission_url")
    elem.clear()
    elem.send_keys(link)

    elem = driver.find_element_by_id("submission_body")
    elem.clear()
    elem.send_keys(content)

    elem = driver.find_element_by_id("submission_submit")
    elem.click()

    wait = WebDriverWait(driver, 30)
    wait.until(EC.title_is(thread_title))

def update_feed_file(new_posts):
    with open('feed_list.json') as json_file:
        feeds = json.load(json_file)

    for i in range(0,len(new_posts)):
        tmp = feeds["Feeds"][i]["last_read_title"]
        feeds["Feeds"][i]["last_read_title"] = new_posts[i][0]["title"]

        tmp = feeds["Feeds"][i]["last_update"]
        feeds["Feeds"][i]["last_update"] = new_posts[i][0]["updated"]

    with open("feed_list.json", "w") as json_file:
        json.dump(feeds, json_file)


def crawl_feed(feed_link, last_read_title, last_update, default_sub):
    feed = feedparser.parse(feed_link)
    new_posts = []
    for e in feed['entries']:

        #Change from naive to aware datetime when needed, can't compare naive w/ aware
        utc=pytz.UTC
        try:
            new_date = utc.localize(parser.parse(e['updated']))
        except ValueError:
            new_date = parser.parse(e['updated'])
        try:
            last_date = utc.localize(parser.parse(last_update))
        except ValueError:
            last_date = parser.parse(last_update)

        if new_date > last_date:
            subraddle = check_sub(default_sub, e)
            new_posts.append({"title":e['title'],"link":e['link'],"updated":e['updated'],"subraddle":subraddle, "new":"true"})
        elif new_date == last_date:
            subraddle = check_sub(default_sub, e)
            new_posts.append({"title":e['title'],"link":e['link'],"updated":e['updated'],"subraddle":subraddle, "new":"false"})
            break
        else:
            break
    return new_posts

def get_rss_pages():
    with open('feed_list.json') as json_file:
        feeds = json.load(json_file)
    return feeds

def main():

    feeds = get_rss_pages()
    all_posts = []
    for i in feeds['Feeds']:
        new_posts = crawl_feed(i['link'],i['last_read_title'],i['last_update'],i['subraddle'])
        all_posts.append(new_posts)


        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_driver = "/home/selver/Projects/Chrome/chromedriver"
        driver = webdriver.Chrome(chrome_options=chrome_options, executable_path=chrome_driver)


        login(driver, i['username'], i['password'])
        for post in new_posts:
            if post["new"] == "true":
                start_new_thread(driver, post['subraddle'], post["title"], "" , post["link"])

        driver.close()

    update_feed_file(all_posts) 

if __name__ == "__main__":
    main()
